resource "cloudflare_zone" "leppezdotcom" {
  zone = var.zone_name
  plan = var.plan
  type = var.type
}

/*
 * Protonmail DNS Records
 */

resource "cloudflare_record" "proton_CNAME1" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_CNAME1_name
  type    = "CNAME"
  value   = var.proton_CNAME1_value
}

resource "cloudflare_record" "proton_CNAME2" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_CNAME2_name
  type    = "CNAME"
  value   = var.proton_CNAME2_value
}

resource "cloudflare_record" "proton_CNAME3" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_CNAME3_name
  type    = "CNAME"
  value   = var.proton_CNAME3_value
}

resource "cloudflare_record" "proton_MX1" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_MX1_name
  type    = "MX"
  value   = var.proton_MX1_value
}

resource "cloudflare_record" "proton_MX2" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_MX2_name
  type    = "MX"
  value   = var.proton_MX2_value
}

resource "cloudflare_record" "proton_TXT1" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_TXT1_name
  type    = "TXT"
  value   = var.proton_TXT1_value
}

resource "cloudflare_record" "proton_TXT2" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_TXT2_name
  type    = "TXT"
  value   = var.proton_TXT2_value
}

resource "cloudflare_record" "proton_TXT3" {
  zone_id = cloudflare_zone.leppezdotcom.id
  name    = var.proton_TXT3_name
  type    = "TXT"
  value   = var.proton_TXT3_value
}

/*
 * Cloudflare Zone settings
 */
resource "cloudflare_zone_settings_override" "leppezdotcom" {
  zone_id = cloudflare_zone.leppezdotcom.id
  settings {
    http3                    = var.http3
    zero_rtt                 = var.zero_rtt
    ipv6                     = var.ipv6
    websockets               = var.websockets
    opportunistic_onion      = var.onion
    pseudo_ipv4              = var.pseudo_ipv4
    ip_geolocation           = var.ip_geolocation
    max_upload               = var.max_upload
    always_use_https         = var.always_use_https
    min_tls_version          = var.min_tls_version
    opportunistic_encryption = var.encryption
    tls_1_3                  = var.tls_1_3
    automatic_https_rewrites = var.automatic_https_rewrites
    cache_level              = var.cache_level
    browser_cache_ttl        = var.browser_cache_ttl
    always_online            = var.always_online
    development_mode         = var.development_mode
  }
}
